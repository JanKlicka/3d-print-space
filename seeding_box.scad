
/// -------------- USED EXAMPLES

//saucer(xlen=3, ylen=3,to=6,gap=2);
//planter(h=50);

sliced_planter(h=50);
translate([-5, 0, 0]) rotate([0,0,180]) sliced_planter(h=50);


//translate([50, 50, 0]) planter(h=40);

// --------- CODE, feel free to use it in your own projects. If you make something cool, send me a link so I can check it out!


//https://www.thingiverse.com/thing:1977298
//from salvador-richter
/*
offset=top-bottom
*/
module pyramid(x=10, y=10, offsetX=5,offsetY=0,z=10)
{
    hull()
    {
        cube([x,y, 0.001], center=true);
        translate([0,0,z]) cube([x+offsetX,y+offsetY, 0.001], center=true);
    }
}
module planter_hook(r, bt, lh, lpch, x=10, y=3, z=3,side) {
    z=lh+lpch;
    if(side == true) {
        rotate([0,0,45]) translate([r-x/2+0.2,0,bt+z/2]) cube([x,y,z+0.1],center=true);
    } else {
        rotate([0,0,45]) translate([-r+x/2-0.2,0,bt+z/2]) cube([x,y,z+0.1],center=true);
    }
}
/*
r=radius
h=height
wt=wall thickness
bt=bottom thickness
dhr=drain hole radius
dhoffset=offset for drain holes (r/2)
lh=leg height
lpch=height of perpendicular cylinder above legs
*/
module planter(r=30, h=30, wt=0.4*3,bt=0.2*4, dhr=3,lh=2,lpch=3) {
    offset=r/2;
    translate([0,0,lh])
    difference() {
        cylinder(r=r+wt,h+bt);
        translate([0,0,bt])cylinder(r=r,h=h+1);
        translate([offset,0,-1])cylinder(r=dhr,h=bt+2);
        translate([-offset,0,-1])cylinder(r=dhr,h=bt+2);
        translate([0,offset,-1])cylinder(r=dhr,h=bt+2);
        translate([0,-offset,-1])cylinder(r=dhr,h=bt+2);
        planter_hook(r=r+1+wt, bt=0,lh=lh,lpch=lpch,x=12,y=4);
        planter_hook(r=r+1+wt, bt=0,lh=lh,lpch=lpch,side=true,x=12,y=4);
        //rotate([0,0,30]) cube([(wt+r)*2+1,0.1,bt*2+1+h*2],center=true);
    }
}
module sliced_planter(r=30, h=30, wt=0.4*3,bt=0.2*4, dhr=3,lh=2,lpch=3) {
    intersection() {
        rotate([0,0,-45]) planter(r=r, h=h, wt=wt,bt=bt, dhr=dhr,lh=lh,lpch=lpch);
        translate([0, -r*2, -h/2]) 
        cube([r*5, r*5, h*5]);
    }
}

module planter_legs(r=30, h=30, wt=0.4*3,bt=0.2*4,lr=3,lh=2) {  
    offset=r/5*4;
    angle=30;
    translate([cos(60)*offset, sin(60)*offset, 0]) cylinder(r=lr, h=lh);
    translate([cos(100)*offset, sin(100)*offset, 0]) cylinder(r=lr, h=lh);
    translate([cos(150)*offset, sin(150)*offset, 0]) cylinder(r=lr, h=lh);
    translate([cos(200)*offset, sin(200)*offset, 0]) cylinder(r=lr, h=lh);
    
    translate([cos(30)*offset, sin(30)*offset, 0]) cylinder(r=lr, h=lh);
    translate([cos(-10)*offset, sin(-10)*offset, 0]) cylinder(r=lr, h=lh);
    translate([cos(-60)*offset, sin(-60)*offset, 0]) cylinder(r=lr, h=lh);
    translate([cos(-110)*offset, sin(-110)*offset, 0]) cylinder(r=lr, h=lh);
    
    cylinder(r=lr, h=lh);
    
    //translate([cos(30)*offset, sin(30)*offset, 0]) cylinder(r=lr, h=lh);
    //translate([-cos(30)*offset, sin(30)*offset, 0]) cylinder(r=lr, h=lh);
    //translate([cos(angle)*offset, -sin(angle)*offset, 0]) cylinder(r=lr, h=lh);
    //translate([-cos(angle)*offset, -sin(angle)*offset, 0]) cylinder(r=lr, h=lh);
}


/*
rlen=row length
xlen,ylen = for nonsquare shapes, rlen overrides xylen
gap=gap between planters
sh=saucer height
to=top offset (how steep the walls will be)
lr=leg radius
lpch=height of perpendicular cylinder above legs
ct=canal thickness
*/
module saucer(/*saucer*/ xlen=2, ylen=2,rlen=0,gap=-1,sh=10,to=3,ct=6,
    /*planter*/ r=30, h=30, wt=0.4*3,bt=0.2*4,lr=3,lh=2,lpch=3) {
    if (rlen > 0) {
        xlen=rlen;
        ylen=rlen;
    }
    d=r*2+wt*2;
    sr=r+wt;//+gap*2;
    difference() {
        for (i=[0:xlen-1]) {
            for (y=[0:ylen-1]) {
                //translate([i*(d+gap),y*(d+gap),0])cylinder(r=sr,h=lh+lpch); // "outter" perpendicular cyl
                //translate([i*(d+gap),y*(d+gap),lh+lpch])cylinder(r1=sr,h=sh-lh-lpch, r2=sr+wt+to); // "outter" cyl
                translate([i*(d+gap),y*(d+gap),0])cylinder(r1=r+wt+2,h=sh, r2=r+wt+wt+to); // "outter" cyl
            }
        }
        for (i=[0:xlen-1]) {
            for (y=[0:ylen-1]) {
                 translate([i*(d+gap),y*(d+gap),bt])cylinder(r=r+wt+1,h=lh+lpch+1); // perpendicular
                 translate([i*(d+gap),y*(d+gap),bt+lh+lpch])cylinder(r1=r+wt+1,h=sh, r2=sr+to); // "inner" cyl
            }
        }
        shorten=3;
        for (i=[0:xlen-1]) {
            translate([i*(d+gap),(ylen-1)/2*(2*r+wt*2+gap)-shorten/2,bt]) pyramid(x=ct,y=ylen*(2*r+wt*2+gap)-2*wt-shorten,offsetX=4,z=sh);
        }
        for (y=[0:ylen-1]) {
            translate([(xlen-1)/2*(2*r+wt*2+gap)-shorten/2,y*(d+gap),bt]) pyramid(x=xlen*(2*r+wt*2+gap)-2*wt-2*shorten,y=ct,offsetY=4,z=sh);
        }
    }   
    for (i=[0:xlen-1]) {
        for (y=[0:ylen-1]) {
            translate([i*(d+gap),y*(d+gap),bt]) {
                planter_legs(r,h,wt,bt,lr,lh);
                planter_hook(r=r+1+wt, bt=bt,lh=lh,lpch=lpch,y=4);
                planter_hook(r=r+1+wt, bt=bt,lh=lh,lpch=lpch,side=true,y=4);
            }
        }
    }
    //translate([r-5+0.1,0,bt+lh+lpch-1.5]) #cube([10,3,3],center=true); // hook for planter
}


